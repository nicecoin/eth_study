# Ganache
ここからインストールして起動
https://truffleframework.com/ganache

# node.js
truffleの実行に必要

## nvm-windows
参考 https://qiita.com/idani/items/53567d92f936846e111c

node.jsのバージョン管理ツール

https://github.com/coreybutler/nvm-windows

nodistの開発が止まっており、使い物にならないのでこちらを使う

### nvm-windowsセットアップ
nvm-setup.zipをダウンロードして展開、nvm-setup.exe実行してセットアップ

インストール先をデフォルトのまま実行するとまともに動作しないのでC:\nvmにインストール

node.jsのセットアップ先もC:\nodejsを指定した

### mac/linux) バージョン管理ツール n インストール 
参考：　https://qiita.com/ksh-fthr/items/c272384f73f8e319733c

一旦、node.jsを普通にインストール
https://nodejs.org/ja/

nをインストール
```
$ sudo npm install -g n
/usr/local/bin/n -> /usr/local/lib/node_modules/n/bin/n
+ n@2.1.12
added 1 package from 4 contributors in 0.692s

```
このままだとnode.jsを使用する時、毎回Sudoが必要
chownでn配下のnode.jsのオーナーをカレントユーザーにする
```
$ sudo chown -R $(whoami) /usr/local/n;
$ sudo chown -R $(whoami) /usr/local/lib/node_modules/
$ sudo chown -R $(whoami) .config
```


## node.js インストール
windows-powershellを管理者で実行

nvmを使用してnode.js 10.15.0をインストール

```
PS C:\nvm> nvm list available

|   CURRENT    |     LTS      |  OLD STABLE  | OLD UNSTABLE |
|--------------|--------------|--------------|--------------|
|    11.6.0    |   10.15.0    |   0.12.18    |   0.11.16    |
|    11.5.0    |   10.14.2    |   0.12.17    |   0.11.15    |
|    11.4.0    |   10.14.1    |   0.12.16    |   0.11.14    |
|    11.3.0    |   10.14.0    |   0.12.15    |   0.11.13    |
|    11.2.0    |   10.13.0    |   0.12.14    |   0.11.12    |
|    11.1.0    |    8.15.0    |   0.12.13    |   0.11.11    |
|    11.0.0    |    8.14.1    |   0.12.12    |   0.11.10    |
|   10.12.0    |    8.14.0    |   0.12.11    |    0.11.9    |
|   10.11.0    |    8.13.0    |   0.12.10    |    0.11.8    |
|   10.10.0    |    8.12.0    |    0.12.9    |    0.11.7    |
|    10.9.0    |    8.11.4    |    0.12.8    |    0.11.6    |
|    10.8.0    |    8.11.3    |    0.12.7    |    0.11.5    |
|    10.7.0    |    8.11.2    |    0.12.6    |    0.11.4    |
|    10.6.0    |    8.11.1    |    0.12.5    |    0.11.3    |
|    10.5.0    |    8.11.0    |    0.12.4    |    0.11.2    |
|    10.4.1    |    8.10.0    |    0.12.3    |    0.11.1    |
|    10.4.0    |    8.9.4     |    0.12.2    |    0.11.0    |
|    10.3.0    |    8.9.3     |    0.12.1    |    0.9.12    |
|    10.2.1    |    8.9.2     |    0.12.0    |    0.9.11    |
|    10.2.0    |    8.9.1     |   0.10.48    |    0.9.10    |

This is a partial list. For a complete list, visit https://nodejs.org/download/release

PS C:\nvm> nvm install 10.15.0
Version 10.15.0 installed.

PS C:\nvm> nvm list
  10.15.0
```

インストールしただけでは使用できないので nvm useでインストールしたnode.js v10.15.0を利用可能にする

```  
PS C:\nvm> nvm use 10.15.0
Now using node v10.15.0 (64-bit)

PS C:\nvm> nvm list
  * 10.15.0 (Currently using 64-bit executable)

PS C:\nvm> node -v
v10.15.0

PS C:\nvm> npm -v
6.4.1
```

### mac/linux) node.js インストール
```
$ n --lts
10.15.0

$ sudo n lts

$ node -v
v10.15.0

$ npm -v
6.4.1
```


# truffle
Ethereum の開発フレームワーク

## truffle セットアップ

windows-build-toolが必要になるので先にinstall
```
> npm install --global --production windows-build-tools
---------- Visual Studio Build Tools ----------
Successfully installed Visual Studio Build Tools.
------------------- Python --------------------
Successfully installed Python 2.7

Now configuring the Visual Studio Build Tools and Python...

All done!

+ windows-build-tools@5.1.0
added 145 packages from 98 contributors in 799.398s
```

truffleをインストール
```
PS C:\nvm> npm install -g truffle
C:\nodejs\truffle -> C:\nodejs\node_modules\truffle\build\cli.bundled.js
+ truffle@5.0.1
updated 1 package in 12.808s
```

### mac/linux)

chownで /usr/local/lib/node_modules/ のオーナーをカレントユーザーにする
```
$ sudo chown -R $(whoami) /usr/local/lib/node_modules/
```

# サンプルプロジェクト作成
nice-coin-sample
https://bitbucket.org/sjtaro001/nice_coin_sample/src/develop/

プロジェクト初期化

```
PS C:\git\nice_coin_sample> truffle init

√ Preparing to download
√ Downloading
√ Cleaning up temporary files
√ Setting up box

Unbox successful. Sweet!

Commands:

  Compile:        truffle compile
  Migrate:        truffle migrate
  Test contracts: truffle test
```

OpenZeppelin ライブラリの導入

```
PS C:\git\nice_coin_sample> npm init -f
npm WARN using --force I sure hope you know what you are doing.
Wrote to C:\git\nice_coin_sample\package.json:

{
  "name": "nice_coin_sample",
  "version": "1.0.0",
  "main": "truffle-config.js",
  "directories": {
    "test": "test"
  },
  "scripts": {
    "test": "echo \"Error: no test specified\" && exit 1"
  },
  "repository": {
    "type": "git",
    "url": "git+https://sjtaro001@bitbucket.org/sjtaro001/nice_coin_sample.git"
  },
  "keywords": [],
  "author": "",
  "license": "ISC",
  "homepage": "https://bitbucket.org/sjtaro001/nice_coin_sample#readme",
  "description": ""
}

PS C:\git\nice_coin_sample> npm install openzeppelin-solidity --save
npm WARN nice_coin_sample@1.0.0 No description

+ openzeppelin-solidity@2.1.1
added 1 package from 1 contributor and audited 2 packages in 2.782s
found 0 vulnerabilities

```

## NiceCoin実装

contracts/NiceCoin.sol
```
pragma solidity ^0.5.0;
//import "github.com/OpenZeppelin/openzeppelin-solidity/contracts/token/ERC20/ERC20.sol";
//import "openzeppelin-solidity/contracts/token/StandardToken.sol";
import "node_modules/openzeppelin-solidity/contracts/token/ERC20/ERC20.sol";

contract NiceCoin is ERC20 {
    string public name = ""; 
    string public symbol = "";
    uint public decimals = 18;
    uint public INITIAL_SUPPLY = 10000 * (10 ** decimals);
    
    address _account = msg.sender;

    constructor(string memory _tokenName, string memory _symbol, uint _decimals ) public {
        name = _tokenName;
        symbol = _symbol;
        decimals = _decimals;
        _mint(_account,INITIAL_SUPPLY);
    }
}

```

## コンパイル
```
PS C:\git\nice_coin_sample> truffle compile
Compiling .\contracts\Migrations.sol...
Compiling .\contracts\NiceCoin.sol...
Compiling node_modules/openzeppelin-solidity/contracts/math/SafeMath.sol...
Compiling node_modules/openzeppelin-solidity/contracts/token/ERC20/ERC20.sol...
Compiling node_modules/openzeppelin-solidity/contracts/token/ERC20/IERC20.sol...
Writing artifacts to .\build\contracts
```

## デプロイ

Ganache と連携するために truffle-config.js を編集

```
module.exports = {
  networks: {
    ganache: {
      host: "localhost",
      port: 7545,
      network_id: "*"
    }
  }
};
```

デプロイ用スクリプト
migrations\2_deploy_nice_coin.js
```
const NiceCoin = artifacts.require('./NiceCoin.sol')

module.exports = (deployer) => {
    const tokenName = "NiceCoin"
    const symbol = "NIC"
    const decimal = 18
    deployer.deploy(NiceCoin, tokenName, symbol, decimal)
}
```

! このまま実行するとtruffle.jsファイルが見つからないためにデプロイに失敗する

上記のtruffle-config.jsをコピーしてtruffle.jsを作成

```
> copy ./truffle-config.js ./truffle.js
```

デプロイ
```
PS C:\git\nice_coin_sample> truffle migrate
⚠️  Important ⚠️
If you're using an HDWalletProvider, it must be Web3 1.0 enabled or your migration will hang.


Starting migrations...
======================
> Network name:    'ganache'
> Network id:      5777
> Block gas limit: 6721975


2_deploy_nice_coin.js
=====================

   Deploying 'NiceCoin'
   --------------------
   > transaction hash:    0xe5044c7f3d9b32a80838f12f63a24d575049f01c06ab75dd2a71e73cd2a8c6e9
   > Blocks: 0            Seconds: 0
   > contract address:    0x4fE0837334ba7070A36c86c4fdBfE9ad58cf23f7
   > account:             0xE6B62674c33cD9984dBbe57b59c96DC175679a3a
   > balance:             99.952061624
   > gas used:            1495230
   > gas price:           20 gwei
   > value sent:          0 ETH
   > total cost:          0.0299046 ETH


   > Saving migration to chain.
   > Saving artifacts
   -------------------------------------
   > Total cost:           0.0299046 ETH


Summary
=======
> Total deployments:   1
> Final cost:          0.0299046 ETH
```

## 動作確認

truffleのconsoleを立ち上げて動作確認

```
PS C:\git\nice_coin_sample> truffle console
```

変数niceCoinにNiceCoinのインスタンス代入
```
truffle(ganache)> var niceCoin; NiceCoin.deployed().then((obj) => { niceCoin = obj; })
undefined
truffle(ganache)> niceCoin.name()
'NiceCoin'
truffle(ganache)> niceCoin.symbol()
'NIC'
```

accountのアドレスを取得
```
truffle(ganache)> var accounts;
undefined
truffle(ganache)> web3.eth.getAccounts(function(err,res) { accounts = res; });
[ '0xE6B62674c33cD9984dBbe57b59c96DC175679a3a',
  '0xde704c652912B4aF1141Ac91AA0aB8BA85dB7718',
  '0x9BfB44d13dc9D58A1a7cB85Ef5a2fD8276893B3C',
  '0x7C0EB5Ba4AC0D46E759d867A87EcBD7c699ECab4',
  '0x7a4673Dc481390bC1B02803dfD44098F80CBbCcB',
  '0x5A24e4d455a184F11A9C7adF1Bdfa0A3E5DC3d22',
  '0xb3efC69464c4172242Cc8deB69A2241633Ad2BAc',
  '0x8933A497a9dbfc167231a98CC8E843679e307872',
  '0x52510970941C7f4615CCA461C55444Ce6C218254',
  '0xdF7920daE8Af244EdEacAB35aBA556653bEf30B8' ]
truffle(ganache)> var account0 = accounts[0];
undefined
truffle(ganache)> var account1 = accounts[1];
undefined
```

account0からaccount1に送金
```
truffle(ganache)> niceCoin.balanceOf(account0)
<BN: 21e19e0c9bab2400000>
truffle(ganache)> niceCoin.balanceOf(account1)
<BN: 0>
truffle(ganache)> niceCoin.transfer(account1, 1000)
{ tx:
   '0x18519d47bd456b65525f5ad624bbe973c10871e072d521c896b393aaf5b95140',
  receipt:
   { transactionHash:
      '0x18519d47bd456b65525f5ad624bbe973c10871e072d521c896b393aaf5b95140',
     transactionIndex: 0,
     blockHash:
      '0x114173ca55b9036c6e566943c4c87b8d74cd51ddea3897dbbe686cb00788a738',
     blockNumber: 6,
     gasUsed: 51419,
     cumulativeGasUsed: 51419,
     contractAddress: null,
     logs: [ [Object] ],
     status: true,
     logsBloom:
      '0x0000000000000000000000000000000000000008000000000000000008000000000000000000000000000000000000000000000
      000000000000000400020000000000000000000000000000800000200002000000000000000000000000000000000000000000000
      000000000000000000000000000000000000001000000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000080000000000000000000000200000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000000000000000002000080000000000',
     rawLogs: [ [Object] ] },
  logs:
   [ { logIndex: 0,
       transactionIndex: 0,
       transactionHash:
        '0x18519d47bd456b65525f5ad624bbe973c10871e072d521c896b393aaf5b95140',
       blockHash:
        '0x114173ca55b9036c6e566943c4c87b8d74cd51ddea3897dbbe686cb00788a738',
       blockNumber: 6,
       address: '0xAd7d1FD4927c12D93162F5Eeea9Cc8fC558D523A',
       type: 'mined',
       id: 'log_8a1ff11d',
       event: 'Transfer',
       args: [Result] } ] }

truffle(ganache)> niceCoin.balanceOf(account0)
<BN: 21e19e0c9bab23ffc18>
truffle(ganache)> niceCoin.balanceOf(account1)
<BN: 3e8>

// 10進で表示
truffle(ganache)> niceCoin.balanceOf(account1).then(function(resp){n=resp.toString(10);console.log(n);})
1000
```


